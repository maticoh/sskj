# README #

### NAMEN REPOZITORIJA ###

V repozitoriju je enostaven program, ki deluje kot slovar. Narejen je bil za predemet Programiranje 2.

### UPORABA ###

Slovar podatke prebira it tekstovne datoteke, kjer so podatki shranjeni v obliki:

geslo
opis 1
opis 2
|

"|" loči gesla med sabo. Program nato izpiše gesla v seznamu, po katerem je moč iskati, in izpiše opise.

Slovar se nahaja v mapi "data" in se ga lahko tudi ročno spreminja.

### Kontakt ###

Avtor: matic-oskar.hajsen@student.fmf.uni-lj.si