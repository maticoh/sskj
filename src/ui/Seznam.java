package ui;

import javax.swing.DefaultListModel;
import javax.swing.JList;

public class Seznam {
	
	// seznam gesel, ki se izpisuje na levi strani okna
	
	// seznam, ki ga dobi iz Slovarja
	String[] seznam;

	public Seznam(String[] args) {
		seznam = args;
	}
	
	// ustvari listmodel
	public DefaultListModel<String> narediModel(){
		DefaultListModel<String> model = new DefaultListModel<String>();
	    for (String element : seznam)
	    	model.addElement(element);
	    return model;
	}
	
	public JList<String> narediList(DefaultListModel<String> mod){
		return new JList<String>(mod);
	}
}
	
	

