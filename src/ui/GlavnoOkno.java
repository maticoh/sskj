package ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.TextArea;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import slovar.Slovar;


public class GlavnoOkno extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Slovar slo = new Slovar(); // slovar z gesli in opisi
	
	String[] selections = Slovar.kljuciArray(); // array gesel
	
	DefaultListModel<String> model = new Seznam(selections).narediModel(); // listmodel za seznam gesel

	JList<String> list = new JList<String>(model); // seznam gesel
	
	TextArea tekst = new TextArea(); // izpis opisov
	
	JTextField iskanje = new JTextField(); // polje za iskanje
	
	public GlavnoOkno() {
		super();
		setTitle("SSKJ");
		setLayout(new GridBagLayout());
	    	    
	    tekst.setText("Dobrodošli");
	    tekst.setEditable(false);
	    
		list.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if ((!arg0.getValueIsAdjusting()) && !model.isEmpty()) {
	                  tekst.setText(Slovar.vrniOpis(list.getSelectedValue()));
	                }
			}
			
		});
	    
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(list);
		
		// seznam gesel
	    GridBagConstraints c = new GridBagConstraints();
		c.gridy = 1;
		c.gridx = 0;
		c.weighty = 1.0;
		c.weightx = 0.0;
		c.fill = GridBagConstraints.VERTICAL;
		scrollPane.setMinimumSize(new Dimension(300,450));
	    add(scrollPane, c);
	    
	   // okno za opise
	    c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 1;
		c.weighty = 1.0;
		c.weightx = 1.0;
		c.gridheight = 2;
		c.fill = GridBagConstraints.BOTH;
		tekst.setMinimumSize(new Dimension(400,500));
	    add(tekst, c);
	    
	    
	    //iskanje ob vnosu
	    iskanje.getDocument().addDocumentListener(new DocumentListener (){

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				posodobiModel();
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				posodobiModel();
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				posodobiModel();
			}
	    	
	    });
	    
	    
	    // okno za iskanje
	    c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 0;
		c.weighty = 0.0;
		c.weightx = 0.0;
		c.fill = GridBagConstraints.HORIZONTAL;
        iskanje.setMinimumSize(new Dimension(300, 50));
	    add(iskanje, c);
	    
	    				
	}
	
	


	
public void posodobiModel(){
	tekst.setText("Dobrodošli");
	model.clear();
	for (String element : Slovar.najdiKljuce(iskanje.getText()))
    	model.addElement(element);
}
}