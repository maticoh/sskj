package slovar;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Bralec {
	
//	Prebere podatke iz tekst filea in si vrstice zapomni kot atribut.
	
	List<String> vrstice;
	
	public Bralec(){
		Charset charset = Charset.forName("UTF-8");
		Path path = FileSystems.getDefault().getPath("data", "slovar.txt");
		try {
		      List<String> lines = Files.readAllLines(path, charset);
		      this.vrstice = lines;
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
	}
	
	
}
