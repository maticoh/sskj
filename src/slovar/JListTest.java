package slovar;
import java.awt.FlowLayout;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;

public class JListTest {
  public static void main(String[] args) {
    JFrame frame = new JFrame("JList Test");
    frame.setLayout(new FlowLayout());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    String[] selections = { "green", "red", "orange", "dark blue" };
    JList list = new JList(selections);
    list.setSelectedIndex(1);
    System.out.println(list.getSelectedValue());
    frame.add(new JScrollPane(list));
    frame.pack();

    for (String s : selections)
    	System.out.println(s);
    
    frame.setVisible(true);
  }
}