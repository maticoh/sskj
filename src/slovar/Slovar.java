package slovar;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Slovar {
	
	// Slovar, ki vsebuje besede in definicije
	
	public static HashMap<String, List<String>> slovar = new HashMap<String, List<String>>();
	public static Set<String> kljuci_set = slovar.keySet();
	public static String[] kljuci_array = kljuci_set.toArray(new String[kljuci_set.size()]);
	
	public Slovar(){
		
		Bralec vir = new Bralec();
		// z Bralcem prebere vrstice iz tekst filea in jih zapi�e kot slovar:
		// geslo je prva vrstica, kasnej�e so opisa, konec oz. za�etek novega gesla pa ozna�uje simbol |
		
		
		int i = 0;
		int j = 0;
		String kljuc = new String();
    	System.out.println(vir.vrstice.size());
		while ( i < vir.vrstice.size()){
			String vrstica = vir.vrstice.get(i);
			i += 1;
			if (vrstica.equals("|")){
				
				j = 0;
				kljuc = new String();
			} 
			else {
				if (j == 0){
					kljuc = vrstica;
					slovar.put(kljuc, new ArrayList<String>());
					j += 1;
				} 
				else {
					slovar.get(kljuc).add(vrstica);
					j += 1;
				}
			}
						
		}
	}	

	
	// vrne mno�ico gesel
	public static SortedSet<String> kljuciSet(){
		SortedSet<String> kljuci_set = new TreeSet<String>();
		kljuci_set.addAll(slovar.keySet());
		return kljuci_set;
	}
	
	
	//i��e ustrezna gesla glede na vne�en string
	public static String[] najdiKljuce(String geslo){
		SortedSet<String> ustrezni = new TreeSet<String>();
		for (String e : Slovar.kljuciArray()){
			if (e.toLowerCase().contains(geslo.toLowerCase())) {
				ustrezni.add(e);
			}
		}
		return ustrezni.toArray(new String[ustrezni.size()]);
	}
	
	// vrne gesla kot array
	public static String[] kljuciArray(){
		String[] kljuci_array = Slovar.kljuciSet().toArray(new String[kljuci_set.size()]);
		return kljuci_array;
	}
	
	public static void printaj(String key){
		System.out.println(slovar.get(key));
	}
	
	
	// vrne opis gesla
	public static String vrniOpis(String key){
		String besedilo = key + ": \n\n";
		for (String vrstica : slovar.get(key)){
			besedilo += "\n" + vrstica +"\n";
		}
		return besedilo;
	}
	
}
